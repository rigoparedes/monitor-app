angular.module('starter.controllers', [])
    .controller('AppCtrl', function ($scope, $rootScope, $state, $ionicPopup, AuthService, tacticService, campaignService) {
        $scope.username = AuthService.username();
        $scope.basicInfo = {
            fromState: "",
            campaigns: [],
            tactics: [],
            tactic: {}
        };

        $scope.setCurrentUsername = function(name) {
            $scope.username = name;
        };
        $scope.logout = function() {
            AuthService.logout();
            $state.go('login');
        };

        $scope.getCampaignsList = function (userId) {
            campaignService.getCampaigns("55255a41e4b040c7984619cb").then(function (campaigns) {
                $rootScope.$broadcast("hideSpinner");
                $scope.basicInfo.campaigns = campaigns;
            }, function () {
                $rootScope.$broadcast("hideSpinner");
                console.log("failed");
            });
        };

        $scope.getTacticsList = function (campaignId, campaignName) {
            tacticService.getTacticsList(campaignId, campaignName).then(function (tactics) {
                $rootScope.$broadcast("hideSpinner");
                $scope.basicInfo.tactics = tactics;
            }, function () {
                $rootScope.$broadcast("hideSpinner");
                console.log("failed");
            });
        };

        $scope.getTactic = function (tacticId) {
            tacticService.getTactic(tacticId).then(function (tactic) {
                $rootScope.$broadcast("hideSpinner");
                $scope.basicInfo.tactic = tactic;
            }, function () {
                $rootScope.$broadcast("hideSpinner");
                console.log("failed");
            });
        };

        $scope.getCampaignsList();
    })
    .controller('todayCtrl', function ($scope, API_SERVER, $http, $state, socket, $timeout, $ionicLoading) {
        $scope.messages = [];
        $ionicLoading.show({
          template: 'Loading...'
        });
        $scope.$on("hideSpinner", function (e, data) {
            $ionicLoading.hide();
        });
        $http.get(API_SERVER + '/notifications').then(
                function (data) {
                    $scope.$broadcast("hideSpinner");
                    $scope.messages = data.data;
                });


        socket.on('notification', function (data) {
            $scope.messages.unshift(JSON.parse(data));
        });

        $scope.doRefresh = function () {
            $timeout(function () {
                //simulate async response
                //$scope.messages.push({id: i++, name: 'New Item ' + Math.floor(Math.random() * 1000) + 4, type: 0});
                $http.get(API_SERVER + '/notifications').then(
                    function (data) {
                        $rootScope.$broadcast("hideSpinner");
                        $scope.messages = data.data;
                    });
                //Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');

            }, 500);

        };

        $scope.viewTactic = function (notification) {
            notification.status = 1;
            $http.put(API_SERVER + '/notifications/' + notification._id.$oid);

            $scope.basicInfo.tactic = {};
            $scope.getTactic(notification.tacticId);
            $scope.basicInfo.fromState = "tab.today";

            $state.go('tab.tactic');
        };
    })
    .controller('LoginCtrl', function ($scope, $state, $ionicLoading, $ionicPopup, AuthService) {
        $scope.user = {};

        $scope.login = function(user) {
            if (user.username) {
                if (user.password) {
                    $ionicLoading.show({
                      template: 'Loading...'
                    });
                    AuthService.login(user.username, user.password).then(function(authenticated) {
                        $state.go('tab.today', {}, {reload: true});
                        $scope.setCurrentUsername(user.username);
                        $ionicLoading.hide();
                    }, function(err) {
                        $ionicLoading.hide();
                        var alertPopup = $ionicPopup.alert({
                            title: 'Login failed!',
                            template: 'Please check your credentials!'
                        });
                    });
                } else {
                    var alertPopup2 = $ionicPopup.alert({
                        title: 'Login failed!',
                        template: 'Enter password'
                    });
                }

            } else {
                var alertPopup1 = $ionicPopup.alert({
                    title: 'Login failed!',
                    template: 'Enter username'
                });
            }
        };
    })

    .controller('allCtrl', function ($scope, $http, $state, API_SERVER, socket, $timeout, $ionicLoading) {
        $ionicLoading.show({
          template: 'Loading...'
        });
        $scope.$on("hideSpinner", function (e, data) {
            $ionicLoading.hide();
        });
        $scope.messages = [];

        notificationService.getNotifications().then(function (messages) {
            $rootScope.$broadcast("hideSpinner");
            $scope.messages = messages;
        });

        socket.on('notification', function (data) {
            $scope.messages.unshift(JSON.parse(data));
        });

        $scope.doRefresh = function () {
            $timeout(function () {
                // Simulate async response
                notificationService.getNotifications().then(function (messages) {
                    $scope.messages = messages;
                });

                // Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');

            }, 500);

        };

        $scope.viewTactic = function (notification) {
            notificationService.readNotification(notification._id.$oid);

            $scope.basicInfo.tactic = {};
            $scope.getTactic(notification.tacticId);
            $scope.basicInfo.fromState = "tab.today";

            $state.go('tab.tactic');
        };
    })

    .controller('allCtrl', function ($scope, $rootScope, $http, $state, $ionicLoading, socket, $timeout, notificationService) {
        $ionicLoading.show({
          template: 'Loading...'
        });
        $scope.$on("hideSpinner", function (e, data) {
            $ionicLoading.hide();
        });

        notificationService.getNotifications().then(function (messages) {
            $rootScope.$broadcast("hideSpinner");
            $scope.messages = messages;
        });

        socket.on('notification', function (data) {
            if (data) {
                $scope.messages.unshift(JSON.parse(data));    
            }            
        });

        $scope.doRefresh = function () {
            $timeout(function () {
                //simulate async response
                notificationService.getNotifications().then(function (messages) {
                    $scope.messages = messages;
                });

                //Stop the ion-refresher from spinning
                $scope.$broadcast('scroll.refreshComplete');

            }, 500);

        };

        $scope.viewTactic = function (notification) {
            notificationService.readNotification(notification._id.$oid);

            $scope.basicInfo.tactic = {};
            $scope.getTactic(notification.tacticId);
            $scope.basicInfo.fromState = "tab.all";

            $state.go('tab.tactic');
        };
    })

    .controller('campaignsCtrl',['$scope', '$state', '$ionicLoading', function($scope, $state, $ionicLoading) {
        $scope.$on("hideSpinner", function (e, data) {
            $ionicLoading.hide();
        });

        $scope.viewTacticsList = function (campaignId, campaignName) {
            $scope.basicInfo.tactics = [];
            $scope.getTacticsList(campaignId, campaignName);
            $scope.basicInfo.fromState = "tab.campaigns";
            $state.go('tab.tactics');
        };
    }])

    .controller('tacticCtrl',['$scope', '$ionicPopup', '$ionicLoading', 'tacticService',  function ($scope, $ionicPopup, $ionicLoading, tacticService) {
        $ionicLoading.show({
          template: 'Loading...'
        });
        $scope.$on("hideSpinner", function (e, data) {
            $ionicLoading.hide();
        });
        $scope.showConfirm = function(tactic) {
           var confirmPopup = $ionicPopup.confirm({
                title: 'Tactic status change',
                template: 'Are you sure you want to change the status of this tactic?'
           });

           confirmPopup.then(function(res) {
                if (res) {
                    tactic.status = tactic.statusSwitch ? "LIVE" : "PAUSED";
                    tacticService.updateTactic(tactic);
                } else {
                    tactic.statusSwitch = !tactic.statusSwitch;
                }
           });
        };
    }])

    .controller('tacticsCtrl',['$scope', '$ionicPopup', 'tacticService', '$ionicLoading', function($scope, $ionicPopup, tacticService, $ionicLoading) {
        $ionicLoading.show({
          template: 'Loading...'
        });
        $scope.$on("hideSpinner", function (e, data) {
            $ionicLoading.hide();
        });
        $scope.toggleGroup = function(group) {
            if ($scope.isGroupShown(group)) {
                $scope.shownGroup = null;
            } else {
                $scope.shownGroup = group;
            }
        };

        $scope.isGroupShown = function(group) {
            return $scope.shownGroup === group;
        };

        $scope.showConfirm = function(tactic) {
           var confirmPopup = $ionicPopup.confirm({
                title: 'Tactic status change',
                template: 'Are you sure you want to change the status of this tactic?'
           });

           confirmPopup.then(function(res) {
                if (res) {
                    tactic.status = tactic.statusSwitch ? "LIVE" : "PAUSED";
                    tacticService.updateTactic(tactic);
                } else {
                    tactic.statusSwitch = !tactic.statusSwitch;
                }
           });
         };
    }]);
