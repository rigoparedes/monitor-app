angular.module('starter.services', [])
    .factory('socket', function (socketFactory, API_SERVER) {
        var myIoSocket = io.connect(API_SERVER + ':8000/', {'forceNew': true});

        mySocket = socketFactory({
            ioSocket: myIoSocket
        });

        return mySocket;
    })
    .factory('campaignService', ['$http', '$q', 'API_SERVER', function($http, $q, API_SERVER){

        var service = {};

        service.getCampaigns = function (userId) {
            var defer = $q.defer();

            $http.get(API_SERVER + "/campaigns/" + userId).success(
                defer.resolve
            ).error(defer.reject);

            return defer.promise;
        };

        return service;
    }])
    .factory('tacticService', ['$http', '$q', 'API_SERVER', function($http, $q, API_SERVER){

        var service = {};

        service.getTacticsList = function (campaignId, campaignName) {
            var defer = $q.defer(),
                timeDiff,
                totalDays,
                expectedGoal,
                i = 0;

            $http.get(API_SERVER + "/campaigns/" + campaignId + "/tactics").success(
                function (tactics) {
                    if (tactics && tactics.length) {
                        tactics.forEach(function (tactic) {
                            tactic.shortribs = tactic.shortribs || 0;
                            // Date & pacing validations
                            if (tactic && tactic.startDate) {
                                tactic.startDateTime = new Date(tactic.startDate).getTime();
                            }

                            if (tactic && tactic.endDate) {
                                tactic.endDateTime = new Date(tactic.endDate).getTime();

                                timeDiff = tactic.endDateTime - (new Date()).getTime();
                                tactic.daysLeft = Math.ceil(timeDiff / (1000 * 3600 * 24));
                            }

                            if (tactic.daysLeft > 0) {
                                tactic.status = tactic.status === 0 ? "PAUSED" : "LIVE";

                                if (tactic && tactic.startDate && tactic.endDate) {
                                    totalDays = Math.ceil((tactic.endDateTime - tactic.startDateTime) / (1000 * 3600 * 24));
                                }

                                if (timeDiff) {
                                    expectedGoal = Math.round((tactic.goal / totalDays) * (totalDays - tactic.daysLeft));
                                }

                                if (expectedGoal) {
                                    tactic.pacing = (tactic.shortribs / expectedGoal * 100).toFixed(2);
                                }
                            } else {
                                tactic.status = "COMPLETED"
                            }

                            // Additional information
                            tactic.statusSwitch = tactic.status == "LIVE";

                            tactic.campaignname = campaignName;
                        });
                    }
                    defer.resolve(tactics)
                }
            ).error(defer.reject);

            return defer.promise;
        };

        service.getTactic = function (tacticId) {
            var defer = $q.defer();

            $http.get(API_SERVER + "/tactic/" + tacticId).success(
                function (tactic) {
                    tactic.shortribs = tactic.shortribs || 0;
                    // Date & pacing validations
                    if (tactic && tactic.startDate) {
                        tactic.startDateTime = new Date(tactic.startDate).getTime();
                    }

                    if (tactic && tactic.endDate) {
                        tactic.endDateTime = new Date(tactic.endDate).getTime();

                        timeDiff = tactic.endDateTime - (new Date()).getTime();
                        tactic.daysLeft = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    }

                    if (tactic.daysLeft > 0) {
                        tactic.status = tactic.status === 0 ? "PAUSED" : "LIVE";

                        if (tactic && tactic.startDate && tactic.endDate) {
                            totalDays = Math.ceil((tactic.endDateTime - tactic.startDateTime) / (1000 * 3600 * 24));
                        }

                        if (timeDiff) {
                            expectedGoal = Math.round((tactic.goal / totalDays) * (totalDays - tactic.daysLeft));
                        }

                        if (expectedGoal) {
                            tactic.pacing = (tactic.shortribs / expectedGoal * 100).toFixed(2);
                        }
                    } else {
                        tactic.status = "COMPLETED"
                    }

                    // Additional information
                    tactic.statusSwitch = tactic.status == "LIVE";

                    defer.resolve(tactic);
                }
            ).error(defer.reject);

            return defer.promise;
        };

        service.updateTactic = function (tactic) {
            var defer = $q.defer(),
                tacticId = tactic._id.$oid,
                data = {
                    tactic: {
                        name: tactic.name,
                        orgid: tactic.orgid,
                        orgname: tactic.orgname,
                        campaignid: tactic.campaignid,
                        status: tactic.status,
                        startDate: tactic.startDate,
                        endDate: tactic.endDate,
                        pacing: tactic.pacing,
                        impressions: tactic.impressions,
                        spend: tactic.spend
                    }
                };

            $http.put(
                API_SERVER + "/campaigns/" + tactic.campaignid + "/tactics/" + tacticId,
                data
            ).success(defer.resolve).error(defer.reject);

            return defer.promise;
        };

        return service;
    }])
    .service('notificationService', ['$http', '$q', 'API_SERVER', function($http, $q, API_SERVER){

        var service = {};

        service.getNotifications = function () {
            var defer = $q.defer();

            $http.get(API_SERVER + '/notifications').then(
                function (data) {
                    defer.resolve(data.data);
                });

            return defer.promise;
        };

        service.readNotification = function (notificationId) {
            var defer = $q.defer();

            $http.put(API_SERVER + '/notifications/' + notificationId);

            return defer.promise;
        }

        return service;
    }])
    .service('AuthService', function ($q, $http, AuthFactory) {
        var LOCAL_TOKEN_KEY = 'yourTokenKey';
        var username = '';
        var isAuthenticated = false;
        var role = '';
        var authToken;

        function loadUserCredentials() {
            var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
            if (token) {
                useCredentials(token);
            }
        }

        function storeUserCredentials(token) {
            window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
            useCredentials(token);
        }

        function useCredentials(token) {
            username = token.split('.')[0];
            isAuthenticated = true;
            authToken = token;

            $http.defaults.headers.common['X-Auth-Token'] = token;
        }

        function destroyUserCredentials() {
            authToken = undefined;
            username = '';
            isAuthenticated = false;
            $http.defaults.headers.common['X-Auth-Token'] = undefined;
            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
        }

        var _login = function (name, pw) {
            return $q(function (resolve, reject) {
                AuthFactory.login(name, pw)
                .then(function(response) {
                    if (response.data.status == "OK") {
                        storeUserCredentials(response.data.token);
                        resolve('Login success.');
                    } else {
                        reject('Login Failed.');
                    }

                });
            });
        };

        var _logout = function () {
            destroyUserCredentials();
        };

        loadUserCredentials();

        return {
            login: _login,
            logout: _logout,
            isAuthenticated: function () {
                return isAuthenticated;
            },
            username: function () {
                return username;
            }
        };
    })
    .factory("AuthFactory", function ($http, API_SERVER) {
        return {
            login: function (username, password) {
                return $http.post(API_SERVER + "/users/login", {
                    username: username,
                    password: password
                });
            }
        }
    });
