angular.module('starter.services', [])
    .factory('socket', function (socketFactory) {
        var myIoSocket = io.connect('localhost:5000');

        mySocket = socketFactory({
            ioSocket: myIoSocket
        });

        return mySocket;
    })
    .factory('campaignService', ['$http', '$q', function($http, $q){

        var service = {};

        service.getCampaigns = function (userId) {
            var defer = $q.defer();

            $http.get("http://hackbackend-teamaol.rhcloud.com/campaigns/" + userId).success(
                defer.resolve
            ).error(defer.reject);

            return defer.promise;
        };

        return service;
    }])
    .factory('tacticService', ['$http', '$q', function($http, $q){

        var service = {};

        service.getTacticsList = function (campaignId, campaignName) {
            var defer = $q.defer();

            $http.get("http://hackbackend-teamaol.rhcloud.com/campaigns/" + campaignId + "/tactics").success(
                function (tactics) {
                    if (tactics && tactics.length) {
                        tactics.forEach(function (tactic) {
                            if (tactic && tactic.startDate) {
                                tactic.formatStartDate = new Date(tactic.startDate).customFormat("#DD#/#MM#/#YYYY# - #hh#:#mm#:#ss#");
                            }

                            if (tactic && tactic.endDate) {
                                tactic.formatEndDate = new Date(tactic.endDate).customFormat("#DD#/#MM#/#YYYY# - #hh#:#mm#:#ss#");

                                timeDiff = Math.abs(tactic.endDate - (new Date()).getTime());
                                tactic.daysLeft = Math.ceil(timeDiff / (1000 * 3600 * 24));
                            }

                            tactic.statusSwitch = tactic.status == "LIVE";

                            tactic.campaignname = campaignName;
                        });
                    }
                    defer.resolve(tactics)
                }
            ).error(defer.reject);

            return defer.promise;
        };

        service.getTactic = function (tacticId) {
            var defer = $q.defer();

            $http.get("http://hackbackend-teamaol.rhcloud.com/tactic/" + tacticId).success(
                function (tactic) {
                    var timeDiff;

                    if (tactic && tactic.startDate) {
                        tactic.formatStartDate = new Date(tactic.startDate).customFormat("#DD#/#MM#/#YYYY# - #hh#:#mm#:#ss#");
                    }

                    if (tactic && tactic.endDate) {
                        tactic.formatEndDate = new Date(tactic.endDate).customFormat("#DD#/#MM#/#YYYY# - #hh#:#mm#:#ss#");

                        timeDiff = Math.abs(tactic.endDate - (new Date()).getTime());
                        tactic.daysLeft = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    }

                    tactic.statusSwitch = tactic.status == "LIVE";

                    defer.resolve(tactic);
                }
            ).error(defer.reject);

            return defer.promise;
        };

        service.updateTactic = function (tactic) {
            var defer = $q.defer(),
                tacticId = tactic["_id"]["$oid"],
                data = {
                    tactic: {
                        name: tactic.name,
                        orgid: tactic.orgid,
                        orgname: tactic.orgname,
                        campaignid: tactic.campaignid,
                        status: tactic.status,
                        startDate: tactic.startDate,
                        endDate: tactic.endDate,
                        pacing: tactic.pacing,
                        impressions: tactic.impressions,
                        spend: tactic.spend
                    }
                };

            $http.put(
                "http://hackbackend-teamaol.rhcloud.com/campaigns/" + tactic.campaignid + "/tactics/" + tacticId,
                data
            ).success(
                function (tactic) {
                    var timeDiff;

                    if (tactic && tactic.startDate) {
                        tactic.formatStartDate = new Date(tactic.startDate).customFormat("#DD#/#MM#/#YYYY# - #hh#:#mm#:#ss#");
                    }

                    if (tactic && tactic.endDate) {
                        tactic.formatEndDate = new Date(tactic.endDate).customFormat("#DD#/#MM#/#YYYY# - #hh#:#mm#:#ss#");

                        timeDiff = Math.abs(tactic.endDate - (new Date()).getTime());
                        tactic.daysLeft = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    }

                    tactic.statusSwitch = tactic.status == "LIVE";

                    defer.resolve(tactic);
                }
            ).error(defer.reject);

            return defer.promise;
        };

        return service;
    }])
    .service('AuthService', function ($q, $http, AuthFactory) {
        var LOCAL_TOKEN_KEY = 'yourTokenKey';
        var username = '';
        var isAuthenticated = false;
        var role = '';
        var authToken;

        function loadUserCredentials() {
            var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
            if (token) {
                useCredentials(token);
            }
        }

        function storeUserCredentials(token) {
            window.localStorage.setItem(LOCAL_TOKEN_KEY, token);
            useCredentials(token);
        }

        function useCredentials(token) {
            username = token.split('.')[0];
            isAuthenticated = true;
            authToken = token;

            $http.defaults.headers.common['X-Auth-Token'] = token;
        }

        function destroyUserCredentials() {
            authToken = undefined;
            username = '';
            isAuthenticated = false;
            $http.defaults.headers.common['X-Auth-Token'] = undefined;
            window.localStorage.removeItem(LOCAL_TOKEN_KEY);
        }

        var _login = function (name, pw) {
            return $q(function (resolve, reject) {
                AuthFactory.login(name, pw)
                .then(function(response) {
                    if (response.data.status == "OK") {
                        storeUserCredentials(response.data.token);
                        resolve('Login success.');
                    } else {
                        reject('Login Failed.');
                    }
                    
                });
            });
        };

        var _logout = function () {
            destroyUserCredentials();
        };

        loadUserCredentials();

        return {
            login: _login,
            logout: _logout,
            isAuthenticated: function () {
                return isAuthenticated;
            },
            username: function () {
                return username;
            }
        };
    })
    .factory("AuthFactory", function ($http) {
        return {
            login: function (username, password) {
                return $http.post("http://hackbackend-teamaol.rhcloud.com/users/login", {
                    username: username,
                    password: password
                });
            }
        }
    });
