// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'btford.socket-io'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {

        // Ionic uses AngularUI Router which uses the concept of states
        // Learn more here: https://github.com/angular-ui/ui-router
        // Set up the various states which the app can be in.
        // Each state's controller can be found in controllers.js
        $stateProvider

            // setup an abstract state for the tabs directive

            .state('login', {
                url: '/login',
                templateUrl: 'templates/login.html',
                controller: 'LoginCtrl'
            })
            .state('tab', {
                url: '/tab',
                abstract: true,
                templateUrl: 'templates/menu.html'
            })

            // Each tab has its own nav history stack:

            .state('tab.today', {
                url: '/page2',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/today.html',
                        controller: 'todayCtrl'
                    }
                }
            })

            .state('tab.all', {
                url: '/page3',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/all.html',
                        controller: 'allCtrl'
                    }
                }
            })
            .state('tab.campaigns', {
                url: "/campaigns",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/campaigns.html",
                        controller: 'campaignsCtrl'
                    }
                }
            })
            .state('tab.tactic', {
                url: "/tactic/:id",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/tacticDetail.html",
                        controller: 'tacticCtrl'
                    }
                }
            })
            .state('tab.tactics', {
                url: "/tactics",
                views: {
                    'menuContent' :{
                        templateUrl: "templates/tactics.html",
                        controller: 'tacticsCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/tab/page2');

    })
    .run(function ($rootScope, $state, AuthService) {
        $rootScope.$on('$stateChangeStart', function (event,next, nextParams, fromState) {
            if (!AuthService.isAuthenticated()) {
                if (next.name !== 'login') {
                    event.preventDefault();
                    $state.go('login');
                }
            }
        });
    });
